<?php
/**
 *竞猜信息model
 **/
if(!defined('BASEPATH')) exit();

class Quizm extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    /**
     * 竞猜信息 获得数据
     * $limit 查询条数
     * $offset 从第几个查
     */
    public function quizList($limit,$offset)
    {
        return $this->db->select('*')->order_by('vs_id','desc')->get('team_quiz',$limit,$offset)->result_array();
    }
    /**
     * 根据传的字段与对应值查询在对应表里的数量
     * $table 查询的表
     * $array 传的数据 array
     */
    public function existField($table,$array)
    {
        return $this->db->select('*')->where($array)->order_by('vs_id','desc')->get($table)->result_array();
    }

    /**
     * 根据传的字段与对应值查询在对应表里的数量
     * $table 查询的表
     * $array 传的数据 array
     */
    public function existWhere($table,$array)
    {
        return $this->db->select('*')->where($array)->get($table)->result_array();
    }
}