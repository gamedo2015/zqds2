<?php
/**
 *签到model
 **/
if(!defined('BASEPATH')) exit();

class Signm extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    /**
     * 签到信息 获得数据
     * $array 搜索条件 array
     * $limit 查询条数
     * $offset 从第几个查
     */
    public function signList($array,$limit,$offset)
    {
        return $this->db->select('*')->where($array)->order_by('pubTime','desc')->get('news',$limit,$offset)->result_array();
    }
    /**
     * 签到信息入库
     * $array 信息 array
     * 
     */
    public function addSign($array)
    {
        $this->db->insert('sign_log',$array);
        return $this->db->insert_id();
    }

    /**
     * 根据用户id编辑签到积分
     * $array 信息组 array
     * $id 用户id 
     * 
     */
    public function editUser($array,$id)
    {
        $this->db->set($array);
        $this->db->where('user_id', $id);
        $this->db->update('user');
        return $this->db->affected_rows();
    }
    /**
     * 根据传的字段与对应值查询在对应表里的数量
     * $table 查询的表
     * $array 传的数据 array
     */
    public function existField($table,$array)
    {
        return $this->db->select('*')->where($array)->get($table)->result_array();
    }
}