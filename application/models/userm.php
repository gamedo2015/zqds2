<?php

if(!defined('BASEPATH')) exit();

    /**
     * 用户model
     *
     */
class Userm extends CI_Model{

	function __construct(){
        parent::__construct();
    }
    /**
     * 用户注册
     * $array 用户信息 array
     * 
     */
    public function addUser($array)
    {
    	$this->db->insert('user',$array);
    	return $this->db->insert_id();
    }
    /**
     * 添加用户预约状态
     * $array 用户信息 array
     * 
     */
    public function addOrder($array)
    {
        $this->db->insert('order_num',$array);
        return $this->db->insert_id();
    }
    /**
     * 添加用户预约成功信息
     * $array 用户信息 array
     * 
     */
    public function addSuccess($array)
    {
        $this->db->insert('order_success',$array);
        return $this->db->insert_id();
    }
    /**
     * 添加用户预约失败信息
     * $array 用户信息 array
     * 
     */
    public function addFail($array)
    {
        $this->db->insert('order_fail',$array);
        return $this->db->insert_id();
    }
    /**
     * 根据用户id编辑用户信息
     * $array 信息组 array
     * $id 用户id 
     * 
     */
    public function editUser($array,$id)
    {
        $this->db->set($array);
        $this->db->where('user_id', $id);
        $this->db->update('user');
        return $this->db->affected_rows();
    }
    /**
     * 根据条件和表名获得信息
     * $table  表名 string
     * $array  条件 array
     * 
     */
    public function existField($table,$array)
    {
        return $this->db->select('*')->where($array)->get($table)->result_array();
    }
}