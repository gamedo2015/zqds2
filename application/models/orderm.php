<?php
/**
 *新闻信息model
 **/
if(!defined('BASEPATH')) exit();

class Orderm extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    /**
     * 新闻信息 获得数据
     * $array 搜索条件 array
     * $limit 查询条数
     * $offset 从第几个查
     */
    public function newsList($array,$limit,$offset)
    {
        return $this->db->select('*')->where($array)->order_by('pubTime','desc')->get('news',$limit,$offset)->result_array();
    }
    /**
     * 预约
     * $array 用户信息 array
     * 
     */
    public function order($array)
    {
        $this->db->insert('user_order',$array);
        return $this->db->insert_id();
    }
    /**
     * 根据传的字段与对应值查询在对应表里的数量
     * $table 查询的表
     * $array 传的数据 array
     */
    public function existField($table,$array)
    {
        return $this->db->select('*')->where($array)->get($table)->result_array();
    }
}