<?php
/**
 *活动model
 **/
if(!defined('BASEPATH')) exit();

class Activitym extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    /**
     * 根据传的字段与对应值查询在对应表里的数量
     * $field 查询的字段
     * $table 查询的表
     * $array 传的条件 array
     */
    public function existField($field,$table,$array)
    {
        return $this->db->select($field)->where($array)->get($table)->result_array();
    }

    /**
     * 根据球队id修改信息
     * $array 信息组 array
     * $id 球队id 
     * 
     */
    public function editNumber($array,$id)
    {
        $this->db->set($array);
        $this->db->where('id', $id);
        $this->db->update('team_num');
    }
}