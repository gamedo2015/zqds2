<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sign extends My_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
    $this->load->model('signm');
	}
	//签到页面
	public function index()
	{
		//获得登陆账号
		$signName = $this->session->userdata('order_name');
		$signName = ($signName) ? $signName : -1 ;
    //通过用户名查询信息
    $info = $this->signm->existField('user',array('user_name' => $signName));
		//获得签到信息
    $sign = $this->signm->existField('sign_log',array('user_id' => $info[0]['user_id']));
    if(!empty($sign))
    {
    	foreach ($sign as $key => $val) 
    	{
    		$signTime[$key] = $val['sign_time'];
    	}
    }
    else
    {
    	$signTime = -1;
    }
		//debug($signTime);
		$this->smarty->assign('signName',$signName);
		$this->smarty->assign('signTime',$signTime);
    $this->smarty->display('activity/sign.html');
	}
	//签到 数据处理
	public function signAjax()
	{
		
    //通过用户名查询信息
    $info = $this->signm->existField('user',array('user_name' => $_POST['name']));
    //获得签到信息
    $sign = $this->signm->existField('sign_log',array('user_id' => $info[0]['user_id']));
    //签到信息转化日期数组
    if(!empty($sign))
    {
    	foreach ($sign as $key => $val) 
    	{
    		$signTime[$key] = date('j',$val['sign_time']);
    	}
    }
    if(!empty($signTime) && in_array($_POST['today'],$signTime))
    {
	    $msg = '您今日已签到！' ;
	    $status = 0;
	    echo json_encode(array('msg' => $msg,'status' => $status));
	    exit();
    }
    $array = array(
    	'user_id' => $info[0]['user_id'],
    	'sign_time' => time()
    	);
    $insertId = $this->signm->addSign($array);
    //签到成功获得一积分
    if($insertId > 0)
    {
    	$signNum = $info[0]['sign_num'] + 1;
    	$array = array(
    		'sign_num' => $signNum
    		);
    	$this->signm->editUser($array,$info[0]['user_id']);
    }
    //debug($array);

    $msg = $insertId > 0 ? '签到成功' : '签到失败' ;
    $status = $insertId > 0 ? 1 : 0;
    echo json_encode(array('msg' => $msg,'status' => $status));
    exit();
	}

}

