<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Success extends My_Controller {

  /**
   * Index Page for this controller.
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct(){
    parent::__construct();
    $this->load->model('userm');
  }
  /**
     * 预约成功
     *
     */
  public function index()
  {
    //获取session,即登录用户名
    $name = $this->session->userdata('order_name');
    //为空值赋值
    $name = empty($name) ? '0' : $name ;
    //通过用户名查询信息
    $info = $this->userm->existField('user',array('user_name' => $name));
    //通过用户id获得用户名次信息
    $list = $this->userm->existField('order_success',array('user_id' => $info[0]['user_id']));
    //前一百具体第几
    $num = $list[0]['order_id'];

    //debug($num);
    $this->smarty->assign('num',$num);
    $this->smarty->assign('info',$info[0]);
    $this->smarty->display('success.html');
  }
  /**
     * 预约成功用户数据处理
     *
     */
  public function successAjax()
  {
    $post = $_POST;
    //判断手机号是否使用
    if(count($this->userm->existField('user',array('tel' => $post['tel'])))>0)
    {
      $msg = '手机号已使用';
      echo json_encode(array('msg' => $msg,'status' => 0));
      exit();
    }
    //入库 用编辑来代替添加
    $array = array(
      'realName' => $post['realName'] ,
      'address' => $post['address'] ,
      'tel' => $post['tel'] , 
      'postcode' => $post['postcode']
      );
    $num = $this->userm->editUser($array,$post['userId']);
    $msg = $num >= 0 ? '提交成功' : '提交失败' ;
    $status = $num >= 0 ? 1 : 0;
    echo json_encode(array('msg' => $msg,'status' => $status));
    exit();
  }
  /**
     * 预约失败
     *
     */
  public function fail()
  {
    //获取session,即登录用户名
    $name = $this->session->userdata('order_name');
    //为空值赋值
    $name = empty($name) ? '0' : $name ;
    //通过用户名查询信息
    $info = $this->userm->existField('user',array('user_name' => $name));
    //通过用户id获得用户名次信息
    $list = $this->userm->existField('order_fail',array('user_id' => $info[0]['user_id']));
    //一百以后具体第几
    $num = $list[0]['order_id'];
    $num = ($num > 999) ? 999 : $num ;
    $this->smarty->assign('num',$num);
    $this->smarty->display('fail.html');
  }
  
}

