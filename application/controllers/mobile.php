<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mobile extends My_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
    $this->load->model('newsm');
	}
	//首页
	public function index()
	{
    $this->smarty->display('mobile/m_index.html');
	}
	//新闻列表
	public function newslist()
	{
		//新闻
		$news = $this->newsm->existField('news',array('pubTime <=' => time(),'type' => 1,'is_show' => 1));
		//公告
		$ask = $this->newsm->existField('news',array('pubTime <=' => time(),'type' => 2,'is_show' => 1));
		//活动
		$act = $this->newsm->existField('news',array('pubTime <=' => time(),'type' => 4,'is_show' => 1));

		$this->smarty->assign('news',$news);
		$this->smarty->assign('ask',$ask);
		$this->smarty->assign('act',$act);
    $this->smarty->display('mobile/m_newslist.html');
	}
	//新闻详情
	public function newsart()
	{
		$newsId = $this->uri->segment(3);
		$info =$this->newsm->existField('news',array('id' => $newsId));

		$this->smarty->assign('info',$info[0]);
    $this->smarty->display('mobile/m_newsart.html');
	}
	//截图页面
	public function picture()
	{
    $this->smarty->display('mobile/m_picture.html');
	}
}

