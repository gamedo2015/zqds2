<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends My_Controller {

  /**
   * Index Page for this controller.
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct(){
    parent::__construct();
    $this->load->model('orderm');
    $this->load->model('newsm');
    $this->load->library('pagination');
  }

  public function index()
  {
    $info = $this->newsm->newsList(array('pubTime <=' => time(),'is_show' => 1),1,0,'pubTime','desc');
    //debug($info);
    $this->smarty->assign('info',$info[0]);
    $this->smarty->display('main.html');
  }
  public function newslist()
  {
    //所有新闻
    $list = $this->newsm->existField('news',array('is_show' => 1, 'pubTime <=' => time()));
    //debug($this->db->last_query());
    $limit = 5;
    $offset = ($this->uri->segment(3,1)-1)*$limit;
    $config['base_url'] = base_url('index/newslist');
    $config['total_rows'] = count($list);
    $config['per_page'] = $limit;
    $config['first_link'] = '首页';
    $config['last_link'] = '尾页';
    //$config['next_link'] = "<a class='pre'></a>";
    //$config['prev_link'] = "<a class='next'></a>";
    $this->pagination->initialize($config);
    $page=$this->pagination->create_links();

    $data = $this->newsm->newsList(array('is_show' => 1, 'pubTime <=' => time()),$limit,$offset,'pubTime','desc');
    //debug($data);
    $this->smarty->assign('data',$data);
    $this->smarty->assign('page',$page);
    $this->smarty->display('newslist.html');
  }
  //新闻详情
  public function newsart()
  {
    $id = $this->uri->segment(3);
    //获取新闻详情
    $list = $this->newsm->existField('news',array('id' => $id));
    //上一篇
    $pre = $this->newsm->newsList(array('is_show' => 1, 'pubTime >' => $list[0]['pubTime']),1,0,'pubTime','asc');
    //下一篇
    $next = $this->newsm->newsList(array('is_show' => 1, 'pubTime <' => $list[0]['pubTime']),1,0,'pubTime','desc');
    //debug($pre);
    $this->smarty->assign('list',$list[0]);
    $this->smarty->assign('pre',$pre[0]);
    $this->smarty->assign('next',$next[0]);
    $this->smarty->display('newsarticle.html');
  }
  public function order()
  {
    $this->smarty->display('order.html');
  }
  /**
     * 预约数据处理
     *
     */
  public function orderAjax()
  {
    $post = $_POST;
    //判断手机号是否使用
    if(count($this->orderm->existField('user_order',array('phone' => $post['phone'])))>0)
    {
      $msg = '手机号已使用';
      echo json_encode(array('msg' => $msg,'status' => 0));
      exit();
    }

    $array = array(
      'name' => $post['name'] ,
      'phone' => $post['phone'] ,
      'type' => $post['type']
      );

    $insertId = $this->orderm->order($array);

    //debug($this->db->last_query());
    $msg = $insertId > 0 ? '预约成功' : '预约失败' ;
    $status = $insertId > 0 ? 1 : 0;
    echo json_encode(array('msg' => $msg,'status' => $status));
    exit();
  }

}

