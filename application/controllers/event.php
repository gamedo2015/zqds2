<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends My_Controller {

  /**
   * Index Page for this controller.
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct(){
    parent::__construct();
    $this->load->model('userm');
  }
  /*
  * 预约礼盒页
  *
  **/
  public function giftbox()
  {
    //获取session,即登录用户名
    $name = $this->session->userdata('order_name');
    //为空值赋值
    $name = empty($name) ? '0' : $name ;
    //通过获取的值查询信息
    $info = $this->userm->existField('user',array('user_name' => $name));
    $this->smarty->assign('info',$info[0]);
    $this->smarty->display('giftbox.html');
  }
  /*
  * 用户预约数据处理
  *
  **/
  public function order()
  {
    //做活动时间限定,注意时间与前端页面giftbox.html保持一致
    if(time() < strtotime('2015-10-09 11:00:00'))
    {
      echo json_encode(array('status' => 'close'));
      exit;
    }
    //获取session,即登录用户名
    $name = $this->session->userdata('order_name');
    //通过用户名查询信息
    $info = $this->userm->existField('user',array('user_name' => $name));
    //通过用户id获取用户预约信息
    $order = $this->userm->existField('order_num',array('user_id' => $info[0]['user_id']));
    //用户已经预约
    if(count($order) > 0)
    {
      //通过用户预约名次获取预约状态 
      $status = ($order[0]['order_id'] <= 100) ? 'success' : 'fail' ;
      echo json_encode(array('status' => $status));
      exit;
    }
    //用户尚未预约 添加用户预约状态信息 获得主键id
    $orderId = $this->userm->addOrder(array('user_id' => $info[0]['user_id']));
    //用户预约信息
    $array = array(
      'user_id' => $info[0]['user_id'] ,
      'order_id' => $orderId
    );
    //预约用户总数
    $num = $this->db->count_all('order_num');
    //预约用户不大于100 添加用户预约信息到预约成功表
    if($num <= 100)
    {
      $this->userm->addSuccess($array);
      $status = 'success';
    }
    //预约用户大于100 添加用户预约信息到预约失败表
    else
    {
      $this->userm->addFail($array);
      $status = 'fail';
    }
    echo json_encode(array('status' => $status));
    exit;
  }
}

