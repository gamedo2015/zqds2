<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends My_Controller {

  /**
   * Index Page for this controller.
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -  
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in 
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct(){
    parent::__construct();
    $this->load->model('userm');
  }
  /**
     * 用户注册
     *
     */
  public function index()
  {
    $this->smarty->display('register.html');
  }
  /**
     * 用户注册数据处理
     *
     */
  public function addUserAjax()
  {
    $post = $_POST;
    //密码加密
    $pwd = md5(md5($post['pwd']).time());
    //判断用户是否存在
    if(count($this->userm->existField('user',array('user_name' => $post['name'])))>0)
    {
      $msg = '用户名已存在';
      echo json_encode(array('msg' => $msg,'status' => 0));
      exit();
    }
    //判断手机号是否使用
    if(count($this->userm->existField('user',array('user_phone' => $post['phone'])))>0)
    {
      $msg = '手机号已使用';
      echo json_encode(array('msg' => $msg,'status' => 0));
      exit();
    }
    //判断身份证号是否使用
    if(count($this->userm->existField('user',array('user_IDcard' => $post['IDcard'])))>0)
    {
      $msg = '身份证号已使用';
      echo json_encode(array('msg' => $msg,'status' => 0));
      exit();
    }

    $array = array(
      'user_name' => $post['name'] ,
      'user_pwd' => $pwd ,
      'pwd_add' => time() ,
      'user_phone' => $post['phone'] ,
      'user_realName' => $post['realName'] , 
      'user_IDcard' => $post['IDcard'] ,
      'user_email' => $post['email']
      );
    $insertId = $this->userm->addUser($array);
    $msg = $insertId > 0 ? '注册成功' : '注册失败' ;
    $status = $insertId > 0 ? 1 : 0;
    echo json_encode(array('msg' => $msg,'status' => $status));
    exit();
  }
  
}

