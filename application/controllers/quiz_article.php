<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quiz_article extends My_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
    $this->load->model('activitym');
    $this->load->model('userm');
	}
	/**
     * 支持球队活动
     * 
     */
	public function index()
	{	
    //竞赛id
    $vsId = $this->uri->segment(3);
    //通过竞赛id获得球队信息
    $team = $this->activitym->existField('*','team_quiz',array('vs_id'=>$vsId));
		//id为球队id 获得球队支持信息
		$left = $this->activitym->existField('*','team_num',array('id'=>$team[0]['left_team']));
		$right = $this->activitym->existField('*','team_num',array('id'=>$team[0]['right_team']));

		//超过99.00 显示在页面定死99.00
		$left_num = ($left[0]['num'] > 99.00) ? 99.00 : $left[0]['num'];
		//显示在页面,浮点2
		$left[0]['num'] = number_format($left_num,2);
		$right_num = ($right[0]['num'] > 99.00) ? 99.00 : $right[0]['num'];
		//显示在页面,浮点2
		$right[0]['num'] = number_format($right_num,2);

    //获取session,即登录用户名
    $name = $this->session->userdata('order_name');
    //为空值赋值
    $name = empty($name) ? '0' : $name ;
    //通过获取的值查询信息
    $info = $this->activitym->existField('*','user',array('user_name' => $name));
		//debug($right);
		$this->smarty->assign('left',$left[0]);
		$this->smarty->assign('right',$right[0]);
    $this->smarty->assign('info',$info[0]);
    $this->smarty->display('activity/quiz_article.html');
	}
	/**
     * 左边球队
     * 
     */
	public function left()
	{
		//获得球队支持初始数量
		$left = $this->activitym->existField('num','team_num',array('id'=>$_POST['status']));
		//每次点击增加0.01
		$num = $left[0]['num']+0.01;
		//修改数据库球队信息
		$this->activitym->editNumber(array('num'=>$num),$_POST['status']);
		//debug($_POST);
		//用户支持球队信息入库
    $this->userm->editUser(array('user_quiz' => $_POST['status']),$_POST['id']);
		//超过99.00 显示在页面定死99.00
		$num = ($num > 99.00) ? 99.00 : $num ;
		//显示在页面,浮点2
		$num = number_format($num,2).'%';

		echo json_encode(array('left'=>$num));
	}
	/**
     * 右边球队
     * 
     */
	public function right()
	{
		//获得球队支持初始数量
		$right = $this->activitym->existField('num','team_num',array('id'=>$_POST['status']));
		//每次点击增加0.01
		$num = $right[0]['num']+0.01;
		//修改数据库球队信息
		$this->activitym->editNumber(array('num'=>$num),$_POST['status']);
		//debug($_POST);
		//用户支持球队信息入库
    $this->userm->editUser(array('user_quiz' => $_POST['status']),$_POST['id']);
		//超过99.00 显示在页面定死99.00
		$num = ($num > 99.00) ? 99.00 : $num ;
		//显示在页面,浮点2
		$num = number_format($num,2).'%';

		echo json_encode(array('right'=>$num));
	}

}

