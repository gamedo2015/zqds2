<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quiz extends My_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
    $this->load->model('quizm');
    $this->load->library('pagination');
	}
  //竞猜列表
	public function index()
	{
		$list = $this->quizm->existField('team_quiz',array());
		$limit = 5;
    $offset = ($this->uri->segment(3,1)-1)*$limit;
    $config['base_url'] = base_url('quiz/index');
    $config['total_rows'] = count($list);
    $config['per_page'] = $limit;
    $config['first_link'] = '首页';
    $config['last_link'] = '尾页';
    $this->pagination->initialize($config);
    $page=$this->pagination->create_links();

    $data = $this->quizm->quizList($limit,$offset);
    foreach ($data as $key => $val) 
    {
      $left = $this->quizm->existWhere('team_num',array('id' => $val['left_team']));
      $right = $this->quizm->existWhere('team_num',array('id' => $val['right_team']));
      $data[$key]['leftTeam'] = $left[0]['name'];
      $data[$key]['leftImg'] = $left[0]['team_img'];
      $data[$key]['rightImg'] = $right[0]['team_img'];
      $data[$key]['rightTeam'] = $right[0]['name'];
    }
    //debug($data);
    $this->smarty->assign('data',$data);
    $this->smarty->assign('page',$page);
    $this->smarty->display('activity/quiz.html');
	}

}

