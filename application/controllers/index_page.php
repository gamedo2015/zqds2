<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index_page extends My_Controller {

	/**
	 * Index Page for this controller.
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();
    $this->load->model('newsm');
	}

	public function index()
	{
    $info = $this->newsm->newsList(array('pubTime <=' => time(),'is_show' => 1),1,0,'pubTime','desc');
    //debug($info);
    $this->smarty->assign('info',$info[0]);
    $this->smarty->display('main.html');
	}

}

