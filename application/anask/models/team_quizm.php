<?php
/**
 *竞猜活动model
 **/
if(!defined('BASEPATH')) exit();

class Team_quizm extends CI_Model{

    function __construct(){
        parent::__construct();
    }

    /**
     * 球队信息 获得数据
     * $array 搜索条件 array
     * $limit 查询条数
     * $offset 从第几个查
     */
    public function quizList($array,$limit,$offset)
    {//join('team_num','team_num.id = team_quiz.left_team')->join('team_num','team_num.id = team_quiz.right_team')->
        return $this->db->select("*")->order_by('vs_id','desc')->like($array)->get('team_quiz',$limit,$offset)->result_array();

    }
    /**
     * 添加球队信息
     * $array 球队信息组 array
     *
     */
    public function addQuiz($array)
    {
        //入库
        $this->db->insert('team_quiz', $array);
        return mysql_insert_id();
    }
    /**
     * 根据球队信息id编辑球队信息
     * $array 球队信息组 array
     * $id 球队id 
     * 
     */
    public function editQuiz($array,$id)
    {
        $this->db->set($array);
        $this->db->where('vs_id', $id);
        $this->db->update('team_quiz');
    }
    /**
     * 根据球队信息id删除球队信息
     * $id 球队id
     *
     */
    public function delQuiz($id)
    {
        $this->db->where_in('vs_id', $id);
        $this->db->delete('team_quiz');
        return $this->db->affected_rows();
    }
    /**
     * 根据传的字段与对应值查询在对应表里的数量
     * $table 查询的表
     * $array 传的数据 array
     */
    public function existField($table,$array)
    {
        return $this->db->select('*')->like($array)->get($table)->result_array();
    }
    /**
     * 根据传的字段与对应值查询在对应表里的数量
     * $table 查询的表
     * $array 传的数据 array
     */
    public function existWhere($table,$array)
    {
        return $this->db->select('*')->where($array)->get($table)->result_array();
    }
}