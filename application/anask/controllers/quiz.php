<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quiz extends Do_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
    parent::__construct();
    $this->load->model('quizm');
    $this->load->library('pagination');

  }
  /**
     * 球队列表
     */
  public function index()
  {
    $this->smarty->display('quiz/teamList.html');
  }
  /**
     * 球队列表数据处理
     */
  public function teamListAjax()
  {
    $post = $_POST;
    
    $data = $this->quizm->existField('team_num',array());
    
    $limit = $post['limit'];//每页展示个数
    $offset = ($this->uri->segment(3,1)-1)*$limit;//偏移值,从第几个开始
    $config['base_url'] = base_url('quiz/teamListAjax');
    $config['total_rows'] = count($data);//球队数量
    $config['per_page'] = $limit;
    $config['first_link'] = '首页';
    $config['last_link'] = '尾页';
    $config['next_link'] = '下一页';
    $config['prev_link'] = '上一页';
    $this->pagination->initialize($config);
    $page=$this->pagination->create_links();
    $list = $this->quizm->teamList(array(),$limit,$offset);

    $status = (empty($list)) ? 0 : 1;
    echo json_encode(array('list' => $list, 'status' => $status, 'page' => $page));
    exit;
  }
  /**
     * 添加球队
     */
  public function addTeam()
  {
    $this->smarty->display('quiz/addTeam.html');
  }
  /**
     * 添加球队处理数据
     */
  public function addTeamAjax()
  {
    $post = $_POST;
    //判断球队是否存在
    if(count($this->quizm->existWhere('team_num',array('name' => $post['name']))) > 0)
    {
      $msg = '该球队已存在,请修改';
      $status = 0 ;
      echo json_encode(array('msg' => $msg , 'status' => $status ));
      exit;
    }

    $array = array(
      'name' => $post['name'] , 
      'play_num' => $post['play_num'] ,  
      'play_form' => $post['play_form'] ,
      'play_level' => $post['play_level'] ,  
      'culture' => $post['culture'] ,
      'team_img' => $post['img']
      );
    $insertId = $this->quizm->addTeam($array);
    $msg = ($insertId >= 1) ? '球队添加成功' : '球队添加失败' ;
    $status = ($insertId >= 1) ? 1 : 0 ;
    echo json_encode(array('msg' => $msg , 'status' => $status ));
    exit;
  }
  /**
     * 编辑球队
     */
  public function editTeam()
  {
    //球队id
    $id = $this->uri->segment(3);
    //球队详情
    $info = $this->quizm->existWhere('team_num',array('id' => $id));

    $this->smarty->assign('info',$info[0]);
    $this->smarty->display('quiz/editTeam.html');
  }
  /**
     * 编辑球队数据处理
     */
  public function editTeamAjax()
  {
    $post = $_POST;
    $array = array(
      'name' => $post['name'] , 
      'play_num' => $post['play_num'] ,  
      'play_form' => $post['play_form'] ,
      'play_level' => $post['play_level'] ,  
      'culture' => $post['culture'] , 
      'team_img' => $post['img']
      );
    //判断球队是否存在
    $team = $this->quizm->existWhere('team_num',array('name' => $post['name']));
    if(count($team) > 0 && $team[0]['id'] != $post['id'])
    {
      $msg = '该球队信息已存在,请修改';
      $status = 0 ;
      echo json_encode(array('msg' => $msg , 'status' => $status ));
      exit;
    }
    
    //通过球队id获得球队信息
    $info=$this->quizm->existWhere('team_num',array('id'=>$post['id']));
    //图片地址
    $img = $_SERVER['DOCUMENT_ROOT'].$info[0]['team_img'];
    if($info[0]['team_img'] && file_exists($img) && $post['img']!=$info[0]['team_img'])
    {
      unlink($_SERVER['DOCUMENT_ROOT'].$info[0]['team_img']);
    }
    $num = $this->quizm->editTeam($array,$post['id']);
    $msg = ($num >= 0) ? '球队编辑成功' : '球队编辑失败' ;
    $status = ($num >= 0) ? 1 : 0 ;
    echo json_encode(array('msg' => $msg , 'status' => $status ));
    exit;
  }
  /**
     * 删除球队
     */
  public function delTeam()
  {
    //球队id
    $id = $this->uri->segment(3);
    //通过球队id获得球队信息
    $info=$this->quizm->existWhere('team_num',array('id'=>$id));
    //通过球队id获得球队对战信息
    $left=$this->quizm->existWhere('team_quiz',array('left_team'=>$id));
    $right=$this->quizm->existWhere('team_quiz',array('right_team'=>$id));
    if($left OR $right)
    {
      echo json_encode(array('msg' => '球队正在竞赛队列,不可删除' , 'status' => 0 ));
      exit;
    }
    //图片地址
    $img = $_SERVER['DOCUMENT_ROOT'].$info[0]['team_img'];
    if($info[0]['team_img'] && file_exists($img))
    {
      unlink($_SERVER['DOCUMENT_ROOT'].$info[0]['team_img']);
    }
    $num = $this->quizm->delTeam($id);
    $msg = ($num >= 0) ? '球队删除成功' : '球队删除失败' ;
    $status = ($num >= 0) ? 1 : 0 ;
    echo json_encode(array('msg' => $msg , 'status' => $status ));
    exit;
  }
}
