<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Team_quiz extends Do_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   *    http://example.com/index.php/welcome
   *  - or -
   *    http://example.com/index.php/welcome/index
   *  - or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  function __construct()
  {
    parent::__construct();
    $this->load->model('team_quizm');
    $this->load->library('pagination');

  }
  /**
     * 竞猜信息列表
     */
  public function index()
  {
    $this->smarty->display('quiz/quizList.html');
  }
  /**
     * 竞猜信息列表数据处理
     */
  public function quizListAjax()
  {
    $post = $_POST;
    
    $data = $this->team_quizm->existField('team_quiz',array());
    
    $limit = $post['limit'];//每页展示个数
    $offset = ($this->uri->segment(3,1)-1)*$limit;//偏移值,从第几个开始
    $config['base_url'] = base_url('team_quiz/quizListAjax');
    $config['total_rows'] = count($data);//竞猜信息数量
    $config['per_page'] = $limit;
    $config['first_link'] = '首页';
    $config['last_link'] = '尾页';
    $config['next_link'] = '下一页';
    $config['prev_link'] = '上一页';
    $this->pagination->initialize($config);
    $page=$this->pagination->create_links();
    $list = $this->team_quizm->quizList(array(),$limit,$offset);
    foreach ($list as $key => $val) 
    {
      $left = $this->team_quizm->existWhere('team_num',array('id' => $val['left_team']));
      $right = $this->team_quizm->existWhere('team_num',array('id' => $val['right_team']));
      $list[$key]['leftTeam'] = $left[0]['name'];
      $list[$key]['rightTeam'] = $right[0]['name'];
    }
    //debug($list);
    $status = (empty($list)) ? 0 : 1;
    echo json_encode(array('list' => $list, 'status' => $status, 'page' => $page));
    exit;
  }
  /**
     * 添加竞猜信息
     */
  public function addQuiz()
  {
    //球队信息
    $team = $this->team_quizm->existWhere('team_num',array());

    $this->smarty->assign('team',$team);
    $this->smarty->display('quiz/addQuiz.html');
  }
  /**
     * 添加竞猜信息处理数据
     */
  public function addQuizAjax()
  {
    $post = $_POST;

    $array = array(
      'left_team' => $post['leftTeam'] ,  
      'right_team' => $post['rightTeam'] ,  
      'vs_remark' => $post['vs_remark']
      );
    $insertId = $this->team_quizm->addQuiz($array);
    $msg = ($insertId >= 1) ? '竞猜信息添加成功' : '竞猜信息添加失败' ;
    $status = ($insertId >= 1) ? 1 : 0 ;
    echo json_encode(array('msg' => $msg , 'status' => $status ));
    exit;
  }
  /**
     * 编辑竞猜信息
     */
  public function editQuiz()
  {
    //竞猜信息id
    $id = $this->uri->segment(3);
    //竞猜信息详情
    $info = $this->team_quizm->existWhere('team_quiz',array('vs_id' => $id));
    //球队信息
    $team = $this->team_quizm->existWhere('team_num',array());

    $this->smarty->assign('info',$info[0]);
    $this->smarty->assign('team',$team);
    $this->smarty->display('quiz/editQuiz.html');
  }
  /**
     * 编辑竞猜信息数据处理
     */
  public function editQuizAjax()
  {
    $post = $_POST;

    $array = array(
      'left_team' => $post['leftTeam'] ,  
      'right_team' => $post['rightTeam'] ,  
      'vs_remark' => $post['vs_remark']
      );
    
    $num = $this->team_quizm->editQuiz($array,$post['vsId']);
    $msg = ($num >= 0) ? '竞猜信息编辑成功' : '竞猜信息编辑失败' ;
    $status = ($num >= 0) ? 1 : 0 ;
    echo json_encode(array('msg' => $msg , 'status' => $status ));
    exit;
  }
  /**
     * 删除竞猜信息
     */
  public function delQuiz()
  {
    //竞猜信息id
    $id = $this->uri->segment(3);

    $num = $this->team_quizm->delQuiz($id);
    $msg = ($num >= 0) ? '竞猜信息删除成功' : '竞猜信息删除失败' ;
    $status = ($num >= 0) ? 1 : 0 ;
    echo json_encode(array('msg' => $msg , 'status' => $status ));
    exit;
  }
}
