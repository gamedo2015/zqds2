$(function()
{
    var form = $('#userInfo');
    form.validate({
        debug: false,
        errorElement: 'span',
        errorClass: 'validate-inline',
        rules: {
            user_name: {
                required: true,
                minlength:6,
                maxlength:30
            },
            user_pwd: {
                required: true,
                minlength:6,
                maxlength:30
            },
            // repwd: {
            //     required: true,
            //     equalTo:'#user_pwd'
            // },
            user_phone: {
                required: true,
                phone:true
            },
            user_realName: {
                required: true,
                namePRC:true
            },
            user_IDcard: {
                required: true,
                IDcard:true
            },
            user_email: {
                required: true,
                email:true
            }
        }
    });
            
    $('#registerSubmit').click(function()
    {
        if (form.valid() == false) {
            return false;
        }
        var name=$('#user_name').val();
        var pwd=$('#user_pwd').val();
        var phone=$('#user_phone').val();
        var realName=$('#user_realName').val();
        var IDcard=$('#user_IDcard').val();
        var email=$('#user_email').val();

        var requestData = [
            "name=",name,'&',
            "pwd=",pwd,'&',
            "phone=",phone,'&',
            "realName=",realName,'&',
            "IDcard=",IDcard,'&',
            "email=",email
        ].join('');

        //console.log(requestData);

        $.ajax({
            url : "/register/addUserAjax",
            type : "post",
            data : requestData,
            dataType :"json",
            success: function(result)
            {
                //console.log(result);
                if(result.status == 0 )
                {
                    alert(result.msg);
                    return;
                }
                setTimeout(function(){
                    alert(result.msg);
                    window.location.reload();
                    
                },1000);
            }
        });
    });
});    