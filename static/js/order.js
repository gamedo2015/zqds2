$(function()
{
    var form = $('#orderInfo');
    form.validate({
        debug: false,
        errorElement: 'span',
        errorClass: 'validate-inline',
        rules: {
            name: {
                required: true,
                namePRC:true
            },
            phone: {
                required: true,
                phone:true
            },
            type: {
                required: true
            }
        }
    });
            
    $('#submit').click(function()
    {
        if (form.valid() == false) {
            return false;
        }
        var name=$('#name').val();
        var phone=$('#phone').val();
        var type=$('#type').val();

        var requestData = [
            "name=",name,'&',
            "phone=",phone,'&',
            "type=",type,
        ].join('');

        //console.log(requestData);

        $.ajax({
            url : "/index/orderAjax",
            type : "post",
            data : requestData,
            dataType :"json",
            success: function(result)
            {
                //console.log(result);
                if(result.status == 0 )
                {
                    alert(result.msg);
                    return;
                }
                setTimeout(function(){
                    alert(result.msg);
                    window.top.location.reload();
                },1000);
            }
        });
    });
});    