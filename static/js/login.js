$(function()
{
    var form = $('#loginInfo');
    form.validate({
        debug: false,
        errorElement: 'span',
        errorClass: 'validate-inline',
        rules: {
            name: {
                required: true
            },
            pwd: {
                required: true
            },
            check: {
                required: true
            }
        }
    });
            
    $('#loginSubmit').click(function()
    {
        if (form.valid() == false) {
            return false;
        }
        var name=$('#name').val();
        var pwd=$('#pwd').val();
        var check=$('#check').val();
        var capImg=$('#capImg').attr('value');

        var requestData = [
            "name=",name,'&',
            "pwd=",pwd,'&',
            "check=",check,'&',
            "capImg=",capImg,
        ].join('');

        //console.log(requestData);

        $.ajax({
            url : "/login/loginAjax",
            type : "post",
            data : requestData,
            dataType :"json",
            success: function(result)
            {
                //console.log(result);
                if(result.status == 0 )
                {
                    alert(result.msg);
                    window.location.reload();
                }
                setTimeout(function(){
                    alert(result.msg);
                    window.top.location.reload();
                },1000);
            }
        });
    });
});    