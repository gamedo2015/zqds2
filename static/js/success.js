$(function()
{
    var form = $('#successInfo');
    form.validate({
        debug: false,
        errorElement: 'span',
        errorClass: 'validate-inline',
        rules: {
            realName: {
                required: true,
                namePRC:true
            },
            address: {
                required: true,
                address:true
            },
            tel: {
                required: true,
                phone:true
            },
            postcode: {
                required: true,
                digits:true
            }
        }
    });
            
    $('#submit').click(function()
    {
        if (form.valid() == false) {
            return false;
        }
        var realName=$('#realName').val();
        var address=$('#address').val();
        var tel=$('#tel').val();
        var postcode=$('#postcode').val();
        var userId=$('#userId').val();

        var requestData = [
            "realName=",realName,'&',
            "address=",address,'&',
            "tel=",tel,'&',
            "postcode=",postcode,'&',
            "userId=",userId,
        ].join('');

        //console.log(requestData);

        $.ajax({
            url : "/success/successAjax",
            type : "post",
            data : requestData,
            dataType :"json",
            success: function(result)
            {
                //console.log(result);
                if(result.status == 0 )
                {
                    alert(result.msg);
                    return;
                }
                setTimeout(function(){
                    alert(result.msg);
                    window.top.location.reload();
                },1000);
            }
        });
    });
});    