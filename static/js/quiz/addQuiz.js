var addQuiz = function(){

    return {
        init: function(){

            var form = $('#submitForm');
            var error = $('.alert-error', form);
            var success = $('.alert-success', form);

            var pageError = $('.alert_page_error', form);
            var pageSuccess = $('.alert_page_success', form);

            var validate = form.validate({
                debug: false,
                doNotHideMessage: true, //this option enables to show the error/success messages on tab switch.
                errorElement: 'span', //default input error message container
                errorClass: 'validate-inline', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    //account
                    rightTeam: {
                        required: true
                    },
                    vs_remark: {
                        required: true
                    }
                },

                messages: { // custom messages for radio buttons and checkboxes
                    'payment[]': {
                        required: "Please select at least one option",
                        minlength: jQuery.format("Please select at least one option")
                    }
                },

                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("name") == "gender") { // for uniform radio buttons, insert the after the given container
                        error.addClass("no-left-padding").insertAfter("#form_gender_error");
                    } else if (element.attr("name") == "payment[]") { // for uniform radio buttons, insert the after the given container
                        error.addClass("no-left-padding").insertAfter("#form_payment_error");
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavoir
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    success.hide();
                    error.show();
                    App.scrollTo(error, -200);
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.help-inline').removeClass('ok'); // display OK icon
                    $(element)
                        .closest('.control-group').removeClass('success').addClass('error'); // set error class to the control group
                },

                unhighlight: function (element) { // revert the change dony by hightlight
                    $(element)
                        .closest('.control-group').removeClass('error'); // set error class to the control group
                },

                success: function (label) {
                    if (label.attr("for") == "gender" || label.attr("for") == "payment[]") { // for checkboxes and radip buttons, no need to show OK icon
                        label
                            .closest('.control-group').removeClass('error').addClass('success');
                        label.remove(); // remove error label here
                    } else { // display success icon for other inputs
                        label
                            .addClass('valid ok') // mark the current input as valid and display OK icon
                            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                    }
                },

                submitHandler: function (form) {
                    success.show();
                    error.hide();
                    //add here some ajax code to submit your form or just call form.submit() if you want to submit the form without ajax
                }

            });

            $('#submit').click(function () {

                pageSuccess.hide();
                pageError.hide();

                if (form.valid() == false) {
                    return false;
                }

                
                //整理需要提交的数据
                var leftTeam = $("#leftTeam").val();
                var rightTeam = $("#rightTeam").val();
                var vs_remark = $("#vs_remark").val();

                var requestData = [
                    "leftTeam=",leftTeam,'&',
                    "rightTeam=",rightTeam,'&',
                    "vs_remark=",vs_remark
                ].join('');
                //console.log(requestData);
                success.hide();
                error.hide();
                //采用异步提交
                $.ajax({
                    url : "/anask/team_quiz/addQuizAjax",
                    type : "post",
                    data : requestData,
                    dataType :"json",
                    success: function(result){
                        if(result.status == 0 ){
                            pageSuccess.hide();
                            //设置msg
                            pageError.text(result.msg);
                            pageError.show();
                            return;
                        }
                        pageError.hide();
                        pageSuccess.text(result.msg);
                        pageSuccess.show();

                        //重置表单
                        validate.resetForm();

                        $("input").val('');
                        $("textarea").val('');
                        setTimeout(function(){
                            pageError.hide();
                            pageSuccess.hide();
                            window.history.go(-1);
                        },3000);
                    }
                });
            });
        }
    }
}();